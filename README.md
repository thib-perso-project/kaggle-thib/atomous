# Moleculas

First competition, in participation with my dear colleagues Noctis & Woezon !

# TO-DO

- Fine tune models one by one
- Facilitate data prep for `train.csv` and `test.csv`
- Create a *.md file that describe ideas and specify files interactions
- Read other kernels

# Scripts
- __molecular_prep.ipynb__ : can be used on kaggle `train.csv`and `test.csv` to prepare data for modelisaiton/prediction
- __basic_model.ipynb__ : fit and predict on this model with basic linear regression
- __basic_model_per_type.ipynb__ : do the same but type by type. As seen in notebooks, the repartitions of each class is much better.
For one model we even have a  negative score.
- __prediction.ipynb__ : load (prepared) test data and save a submission file for the `basic_model_per_type.ipynb` file.
- __utils.py__ : file with functions and values useful in the while repo.

# Submissions

- 06/25 : 1.97 (position 1160)
One model per type and for each of them a sample red. Not fine tuned.
A little better than sample_submission but not very much

# Storage

Data are in a separate folder.
Architecture is the following

- pickle : folder with all pickle files
- champs-scalar-coupling : all data from kaggle
- submission files